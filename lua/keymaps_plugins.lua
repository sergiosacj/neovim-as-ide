-- Modes
-- "n" normal_mode
-- "i" insert_mode
-- "v" visual_mode
-- "x" visual_block_mode
-- "t" term_mode
-- "c" command_mode

-- Shorten function name
local keymap = vim.keymap.set

-- Silent keymap option
local opts = { silent = true }

-- NvimTree
keymap("n", "<leader>e", ":NvimTreeToggle<CR>", opts)

-- Barbar
keymap('n', 'H', '<Cmd>BufferPrevious<CR>', opts)
keymap('n', 'L', '<Cmd>BufferNext<CR>', opts)
keymap('n', '<C-n>', '<Cmd>BufferMovePrevious<CR>', opts)
keymap('n', '<C-m>', '<Cmd>BufferMoveNext<CR>', opts)
keymap('n', '<leader>c', '<Cmd>BufferClose<CR>', opts)
