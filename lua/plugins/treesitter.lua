local M = {
  "nvim-treesitter/nvim-treesitter",
  commit = "226c1475a46a2ef6d840af9caa0117a439465500",
  event = "BufReadPost",
  dependencies = {
    {
      "JoosepAlviste/nvim-ts-context-commentstring",
      event = "VeryLazy",
      commit = "729d83ecb990dc2b30272833c213cc6d49ed5214",
    },
    {
      "nvim-tree/nvim-web-devicons",
      event = "VeryLazy",
    },
  },
}

function M.config()
  require "nvim-treesitter.configs".setup {
    -- put the language you want in this array
    ensure_installed = require 'helpers.treesitter_langs',

    -- install languages synchronously (only applied to `ensure_installed`)
    sync_install = false,

    highlight = {
      enable = true,       -- false will disable the whole extension
    },

    autopairs = {
      enable = true,
    },

    indent = { enable = true, disable = { "python", "css" } },

    context_commentstring = {
      enable = true,
      enable_autocmd = false,
    },
  }
end

return M
