local M = {
  "akinsho/toggleterm.nvim",
  event = "BufEnter",
}

function M.config()
  require("toggleterm").setup{}
end

return M
