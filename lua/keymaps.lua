-- Modes
-- "n" normal_mode
-- "i" insert_mode
-- "v" visual_mode
-- "x" visual_block_mode
-- "t" term_mode
-- "c" command_mode

-- Shorten function name
local keymap = vim.keymap.set

-- Silent keymap option
local opts = { silent = true }

--Remap space as leader key
vim.g.mapleader = " "

-- Better window navigation
keymap("n", "<C-h>", "<C-w>h", opts)
keymap("n", "<C-j>", "<C-w>j", opts)
keymap("n", "<C-k>", "<C-w>k", opts)
keymap("n", "<C-l>", "<C-w>l", opts)

-- Resize with arrows
keymap("n", "<C-Up>", ":resize -2<CR>", opts)
keymap("n", "<C-Down>", ":resize +2<CR>", opts)
keymap("n", "<C-Left>", ":vertical resize -2<CR>", opts)
keymap("n", "<C-Right>", ":vertical resize +2<CR>", opts)

-- Move text up and down
keymap("x", "J", ":move '>+1<CR>gv-gv", opts)
keymap("x", "K", ":move '<-2<CR>gv-gv", opts)

-- I want my cursor to stay in place
keymap("n", "J", "mzJ`z", opts)

-- Toggle highlights
keymap("n", "<leader>h", "<cmd>set invhlsearch<CR>", opts)

-- Better paste
keymap("v", "p", "P", opts)

-- Stay in indent mode
keymap("v", "<", "<gv", opts)
keymap("v", ">", ">gv", opts)

-- Change word selected
keymap("v", "<leader>r", "\"hy:%s/<C-r>h//g<left><left>", opts)

-- Save file
keymap("n", "<leader>w", ":w<CR>", opts)

-- Close file
keymap("n", "<leader>q", ":q<CR>", opts)
keymap("n", "<leader>Q", ":q!<CR>", opts)
